package com.associations;

import com.associations.entity.Laptop;
import com.associations.entity.Student;
import com.associations.repository.StudentRepository;
import com.associations.repository.LaptopRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class CourseRepositoryTest {

    @Autowired
    LaptopRepository laptopRepository;

    @Autowired
    StudentRepository studentRepository;


    @Test
    public void saveLaptop() {

        Laptop laptop = new Laptop();
        laptop.setLaptopName("Lenovo");
        laptop.setProcessor("I5");

        Laptop laptop1 = new Laptop();
        laptop1.setLaptopName("Dell");
        laptop1.setProcessor("I3");


        List<Laptop> laptops = new ArrayList<>();
        laptops.add(laptop);
        laptops.add(laptop1);

        Student student = new Student();
        student.setMarks(200);
        student.setName("Shaik");


        student.setLaptops(laptops);

        studentRepository.save(student);
    }


    @Test
    public void getLaptop() {

        Laptop laptop = laptopRepository.findById(55).get();

        System.out.println(laptop);
    }
}
