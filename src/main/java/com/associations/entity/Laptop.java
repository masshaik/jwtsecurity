package com.associations.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "laptop")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Laptop {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int lid;

    private String laptopName;

    private String processor;


    @ManyToOne(cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    private Student student;

}
